package br.com.neppo;

public class MathUtil {

    /**
     * Dado um conjunto de n�meros inteiros "ints" e um n�mero arbitr�rio "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */

    // Este algoritmo apresenta uma solu��o recursiva para o problema SubSet Sum, como para cada posi��o do vetor temos
    // duas chamadas a complexidade deste algoritmo � 2^n

    public static boolean subsetSumChecker(int ints[], int sum, int somatual, int tamanho) throws Exception {

        if (ints == null) {// dispara a exe��o caso o objeto seja nulo
            throw new IllegalArgumentException("O conjunto n�o pode ser Nulo!");
        }
        if (sum <= 0) {// ferifica se a soma � v�lida
            return false;
        }
        if (somatual == sum) {// condi��o para parada da recursividade se encontrar o numero desejado
            return true;
        } else {

            tamanho--;// decrementa o tamnaho para que os outros valores do vetor sejam somados

            if (tamanho >= 0) {
                /*chamada recursiva do m�todo que retorna verdadeiro se tiver encontrado ou
                retorna a posi��o do tamanho para somar outros valores
                */
                return subsetSumChecker(ints, sum, somatual, tamanho) || subsetSumChecker(ints, sum, somatual + ints[tamanho], tamanho);
            } else {
                return false;
            }
        }

    }

    public static void main(String[] args) throws Exception {

        //int ints[] = {10, 20, 30, 15};
        //int sum = 0;
        //int somatual = 0;

        //int ints[] = {5, 32, 4, 15, 26, 10};
        //int sum = 68;
        //int somatual = 0;

        //int ints[] = {5, 32, 4, 15, 26, 10, 55, 101, 1344};
        //int sum = 1414;
        //int somatual = 0;

        int ints[] = {1, 5, 2, 3, 6, 4};
        int sum = 21;
        int somatual = 0;

        //int ints[] = null;
        //int sum = 0;
        //int somatual = 0;

        if (MathUtil.subsetSumChecker(ints, sum, somatual, ints.length)) {
            System.out.println("Existe a soma no conjunto!");
        } else {
            System.out.println("N�o existe a soma no conjunto!");
        }

    }
}
